const Prestamo = require("../../models/Prestamo.js");
let Socio = require("../../models/Prestamo.js");

exports.prestamo_list = function (req, res) {
    Prestamo.allPrestamos(function (err, prestar){
        if (err) { 
            res.status(500).send(err.message);
        }
        res.status(200).json({
            prestamo: prestar
        });
    });
};