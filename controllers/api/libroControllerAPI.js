let Libro = require("../../models/Libro");

exports.libro_list = function (req, res) {
    Libro.allLibros(function (err, libros){
        if (err) { 
            res.status(500).send(err.message);
        }
        res.status(200).json({
            libros: libros
        });
    });
};

exports.libro_create = function (req, res) {
    let libro = new Libro ({
        libroID: req.body.libroID,
        titulo: req.body.titulo,
        autor: req.body.autor,
        genero: req.body.genero
    });

    Libro.add(libro, function (err, newLibro) {
        if (err) {
            res.status(500).send(err.message);
        }
        res.status(201).send(newLibro);
    });
};

exports.libro_delete = function(req,res){
    Libro.removeById(req.body._id, function(err, libro){
        if (err) {
            res.status(500).send(err.message);
        }
        res.status(201).send();
    })
};

exports.libro_update = function(req,res){
    Libro.findById(req.body._id, req.body, function(err, libro){
        if (err) {
            res.status(500).send(err.message);
        }
        res.status(201).send(libro);
    })
}
