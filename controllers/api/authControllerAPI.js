var Socio = require('../../models/Socio');
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');

module.exports = {
    authenticate: function (req, res, next) {
        Socio.findOne({email: req.body.email }, function (err, socioInfo) {
            if (err) {
                next(err);
            } else {
                if (socioInfo === null) {
                    return res.status(401).json({status: 'error', message: "Correo o password incorrecto!", data: null });
                }
                if (socioInfo != null && bcrypt.compareSync(req.body.password, userInfo.password)) {
                    var token = jwt.sign({id: socioInfo._id }, req.app.get('secretkey'), { expiresIn: '7d'});
                    res.status(200).json({message: 'Socio encontrado!', data:{socio:socioInfo, token: token}});
                } else {
                    res.status(401).json({status: 'error', message: 'Correo o clave incorrecta', data: null });
                }
            }
        });
    }
}

