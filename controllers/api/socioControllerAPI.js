let Socio = require("../../models/Socio");

exports.socio_prestar = function(req, res){
    Socio.findById(req.body._id, function(err, socio){
        if(err) res.status(500).send(err.message);
        console.log(socio);
        socio.prestar(req.body.libro_id, req.body.desde, req.body.hasta, function(err){
            console.log("Prestado");
            res.status(200).send();
        });
    });
};

exports.socio_list = function (req, res) {
    Socio.allSocios(function (err, socio){
        if (err) { 
            res.status(500).send(err.message);
        }
        res.status(200).json({
            socio: socio
        });
    });
};

exports.socio_create = function (req, res) {
    let socio = new Socio ({
        nombre: req.body.nombre
    });

    Socio.add(socio, function (err, newSocio) {
        if (err) {
            res.status(500).send(err.message);
        }
        res.status(201).send(newSocio);
    });
};