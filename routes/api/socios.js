const { Router } = require("express");
let express = require("express");
let router = express.Router();
let socioConttrollerAPI = require("../../controllers/api/socioControllerAPI");


router.post("/prestar", socioConttrollerAPI.socio_prestar);

router.get("/",socioConttrollerAPI.socio_list);
router.post("/create", socioConttrollerAPI.socio_create);

module.exports = router;