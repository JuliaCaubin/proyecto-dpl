var express = require('express'); 
var authControllerAPI = require('../../controllers/api/authControllerAPI');
var router = express.Router();

var authController = require('../../controllers/api/authControllerAPI');

router.post('/authenticate', authControllerAPI.authenticate);

module.exports = router;