let express = require('express');
let router = express.Router();
let libroControllerAPI = require("../../controllers/api/libroControllerAPI");

router.get("/",libroControllerAPI.libro_list);
router.post("/create", libroControllerAPI.libro_create);
router.delete("/delete", libroControllerAPI.libro_delete);
router.put("/update", libroControllerAPI.libro_update);

module.exports = router;