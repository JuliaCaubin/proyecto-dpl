const { Router } = require("express");
let express = require("express");
let router = express.Router();
let prestamoControllerAPI = require("../../controllers/api/prestamoControllerAPI");

router.get("/",prestamoControllerAPI.prestamo_list);

module.exports = router;