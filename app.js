var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

var librosAPIRouter = require('./routes/api/libros');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Proyecto', { useUnifiedTopology: true, useNewUrlParser: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on("error", console.error.bind('Error de conexion con MongoDB'));
var socio = require('./routes/api/socios');
var prestamo = require('./routes/api/prestamo');
var jwt = require("jsonwebtoken");
//var authAPIRouter = require('./routes/api/auth');
app.set('secretkey', 'Julia_F_13!!666');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//app.use('/api/auth', authAPIRouter);
app.use('/api/libros',/*validarSocio,*/ librosAPIRouter);
app.use('/api/socios', socio);
app.use('/api/prestamo', prestamo);

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


function validarSocio(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretkey'), function (err, decoded) {
    if (err) {
      res.json({ status: "error", message: err.message, data: null });
    } else {
      req.body.socioId = decoded.id;
      console.log('jwt verify: ' + decoded);
      next();
    }
  });
}



  module.exports = app;
