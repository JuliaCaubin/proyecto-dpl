let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let libroSchema = new Schema({
    libroID: Number,
    titulo: String,
    autor: String,
    genero: String
});

//constructor pasandole por parametros 
let Libro = function (id, titulo, autor, genero){
    this.id = id;
    this.titulo = titulo;
    this.autor = autor;
    this.genero = genero;
}

libroSchema.statics.allLibros = function (cb) {
    return this.find( {}, cb);
};

libroSchema.statics.add = function (aLibro, cb) {
    return this.create(aLibro, cb);
};

libroSchema.statics.removeById = function (aLibroId, cb){
    return this.findByIdAndDelete(aLibroId, cb);
}

libroSchema.statics.findById = function (aLibroId, libro, cb){
    return this.findByIdAndUpdate(aLibroId, libro, cb);
}

module.exports = mongoose.model ("Libro", libroSchema);