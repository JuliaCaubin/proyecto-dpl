let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let prestamoSchema = new Schema ({
    desde: Date,
    hasta: Date,
    libro: {type: mongoose.Schema.Types.ObjectId, ref: "Libro"},
    socio: {type: mongoose.Schema.Types.ObjectId, ref: "Socio"}
});

//Cuantos dias esta alquilando el libro
prestamoSchema.methods.diasDePrestamo = function() {
    return moment(this.hasta.diff(moment(this.desde), "days") + 1);
};

prestamoSchema.statics.allPrestamos = function (cb) {
    return this.find( {}, cb);
};

module.exports = mongoose.model ("Prestamo", prestamoSchema);