let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let Prestamo = require("./Prestamo");

let socioSchema = new Schema ({
    nombre: String
});

socioSchema.methods.prestar = function(libroId, desde, hasta, cb){
    let prestamo = new Prestamo({socio: this._id, libro: libroId, desde: desde, hasta: hasta});
    console.log(prestamo);
    prestamo.save(cb);
};

socioSchema.statics.allSocios = function (cb) {
    return this.find( {}, cb);
};

socioSchema.statics.add = function (aSocio, cb) {
    return this.create(aSocio, cb);
};

module.exports = mongoose.model ("Socio", socioSchema);